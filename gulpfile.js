var gulp           = require('gulp'),
	gutil          = require('gulp-util' ),
	sass           = require('gulp-sass'),
	browserSync    = require('browser-sync'),
	cleanCSS       = require('gulp-clean-css'),
	rename         = require('gulp-rename'),
	del            = require('del'),
	imagemin       = require('gulp-imagemin'),
	cache          = require('gulp-cache'),
	autoprefixer   = require('gulp-autoprefixer'),
	notify         = require("gulp-notify");

	gulp.task('browser-sync', function() {
		browserSync({
			server: {
				baseDir: 'static'
			},
			notify: false,
			// tunnel: true,
			// tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
		});
	});
// Пользовательские скрипты проекта

gulp.task('sass', function() {
	return gulp.src('static/sass/main.sass')
	.pipe(sass({outputStyle: 'expanded'}).on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleanCSS()) // Опционально, закомментировать при отладке
	.pipe(gulp.dest('static/css'))
	.pipe(browserSync.stream())
});

gulp.task('watch', ['sass', 'browser-sync'], function() {
	gulp.watch('static/sass/**/*.sass', ['sass']);
	gulp.watch('static/*.html', browserSync.reload);
});

gulp.task('imagemin', function() {
	return gulp.src('static/img/**/*')
	.pipe(cache(imagemin([
		imagemin.optipng({optimizationLeve: 3}),
		imagemin.jpegtran({progressive: true}),
		imagemin.svgo()
	]))) // Cache Images
	.pipe(gulp.dest('assets/img')); 
});

gulp.task('build', ['removeassets', 'imagemin', 'sass'], function() {

	var buildFiles = gulp.src([
		'static/*.html',
		]).pipe(gulp.dest('assets'));

	var buildCss = gulp.src([
		'static/css/*.css',
		]).pipe(gulp.dest('assets/css'));


	var buildFonts = gulp.src([
		'static/fonts/**/*',
		]).pipe(gulp.dest('assets/fonts'));

});


gulp.task('removeassets', function() { return del.sync('assets'); });
gulp.task('clearcache', function () { return cache.clearAll(); });